seqtools (4.44.1+dfsg-8) UNRELEASED; urgency=medium

  * Standards-Version: 4.6.2 (routine-update)
  * Add patch removing dependency from libgtk2.0-dev - unfortunately this
    removes all binaries this package is shipping and the patch is
    useless and will not fix #967743.

 -- Andreas Tille <tille@debian.org>  Sat, 09 Dec 2023 16:53:48 +0100

seqtools (4.44.1+dfsg-7.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062891

 -- Benjamin Drung <bdrung@debian.org>  Thu, 29 Feb 2024 15:46:53 +0000

seqtools (4.44.1+dfsg-7) unstable; urgency=medium

  * Versioned Build-Depends: d-shlibs (>= 0.80)
  * Drop needless override in d-shlibs
    Closes: #999790
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 17 Nov 2021 08:47:38 +0100

seqtools (4.44.1+dfsg-6) unstable; urgency=medium

  * Ignore one flaky test on armhf
    Closes: #972266

 -- Andreas Tille <tille@debian.org>  Thu, 15 Oct 2020 16:10:33 +0200

seqtools (4.44.1+dfsg-5) unstable; urgency=medium

  * Do not ship Makefile* to enable reproducible builds
    Closes: #962589

 -- Andreas Tille <tille@debian.org>  Wed, 10 Jun 2020 14:13:11 +0200

seqtools (4.44.1+dfsg-4) unstable; urgency=medium

  [ Michael R. Crusoe ]
  * Team upload.
  * Multi-Arch: same for libgbtools{0,dev} courtesy the Multiarch
    hinter.

  [ Steffen Moeller ]
  * d/u/metadata: yamllint

  [ Nilesh Patra ]
  * Add autopkgtests
  * Install relevant examples
  * Fix with cme
  * s/http/https

  [ Andreas Tille ]
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Install docs
  * Use Breaks instead of Conflicts

 -- Nilesh Patra <npatra974@gmail.com>  Tue, 09 Jun 2020 23:06:28 +0530

seqtools (4.44.1+dfsg-3) unstable; urgency=medium

  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Thu, 03 Jan 2019 15:28:58 +0100

seqtools (4.44.1+dfsg-2) unstable; urgency=medium

  * Versioned Conflicts+Replaces to enable installing old transitional packages
    Closes: #887616
  * Standards-Version: 4.1.3
  * debhelper 11

 -- Andreas Tille <tille@debian.org>  Mon, 22 Jan 2018 13:25:58 +0100

seqtools (4.44.1+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #879840)

 -- Andreas Tille <tille@debian.org>  Fri, 01 Dec 2017 16:16:44 +0100
